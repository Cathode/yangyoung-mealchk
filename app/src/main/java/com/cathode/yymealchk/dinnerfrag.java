package com.cathode.yymealchk;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cathode.yymealchk.mealview.mealAdapter;

/**
 * Created by lemmondev on 2015-04-10.
 */
public class dinnerfrag extends Fragment {
    public View view;
    RecyclerView meallist;
    public static dinnerfrag mContextdinner;
    public mealAdapter ad;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.mContextdinner=this;

        view = inflater.inflate(R.layout.dinnerfrag,container,false);
        meallist= (RecyclerView) view.findViewById(R.id.dinnerrecy); //dinner
        try {
            ad= new mealAdapter(false,0);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        meallist.setAdapter(ad);
        meallist.setItemAnimator(new DefaultItemAnimator());
        meallist.setLayoutManager(new LinearLayoutManager(this.getActivity()));

        return view;


    }

    public void refreshdinner(){

        try {
            ad= new mealAdapter(false,0);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        meallist.setAdapter(ad);

    }
}

