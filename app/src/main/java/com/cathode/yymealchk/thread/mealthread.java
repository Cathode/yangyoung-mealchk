package com.cathode.yymealchk.thread;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.cathode.yymealchk.MainActivity;
import com.cathode.yymealchk.mealview.mealAdapter;

import java.util.ArrayList;

import toast.library.meal.MealLibrary;

/**
 * Created by lemmondev on 2015-04-10.
 */
public class mealthread extends Thread {




    public static mealthread threadcon;
    public SQLiteDatabase mDb;
    Cursor mCursor;
    ArrayList<String> mealsqldata;
    ArrayList<String> dinnermeal;
    ArrayList<String> lunchcal;
    ArrayList<String> dinnercal;
    ArrayList<String> mealdate;

    public String []mealarry;
    public String []calarry;
    public String []dinnerarry;
    public String []dinnercalarry;
    public String []date;
    public Boolean infochk=true;

    public int mode=0;

        public mealthread(){


        }

        public void run(){

            mDb= MainActivity.mMaincon.mDbhelper.getWritableDatabase();

          threadcon=this;
                if (mode==1){
                    Log.d("Refresh Start!!","");
                    mDb.execSQL("delete from mealist");
                    infochk=false;

                }


           /*SQL 에서 불러오기 */
            mealsqldata= new ArrayList<String>();
            dinnermeal= new ArrayList<String>();
            dinnercal= new ArrayList<String>();
            lunchcal= new ArrayList<String>();
            mealdate= new ArrayList<String>();

            if (!infochk){

                getMealdata();
                Log.d("Load","Data1");
                saveinsql();
                Log.d("Load","Data2");
            }


            refreshlunch();
            refreshdinner();
            mealAdapter.mContext.date= mealdate.toArray(new String[mealdate.size()]); // 날짜
            mealAdapter.mContext.calarry=lunchcal.toArray(new String[lunchcal.size()]); // 점심 칼로리
            mealAdapter.mContext.mealarry=  mealsqldata.toArray(new String[mealsqldata.size()]); // 점심 식사 목록
            mealAdapter.mContext.dinnerarry= dinnermeal.toArray(new String[dinnermeal.size()]); // 저녁 식사 목록
            mealAdapter.mContext.dinnercalarry=dinnercal.toArray(new String[dinnercal.size()]); // 저녁 식사 칼로리

            Log.d("Meal Thread:","Compelete");

        }
        public void refreshlunch(){
            String lunchsql="select * from mealist where type=2";

            mCursor= mDb.rawQuery(lunchsql,null);

            for (int i=0;i<mCursor.getCount();i++){
                mCursor.moveToNext();


                mealdate.add(mCursor.getString(0));
                mealsqldata.add(mCursor.getString(1));
                lunchcal.add(mCursor.getString(2));

            }


        }

    public void refreshdinner(){

        String dinnersql="select * from mealist where type=3";

        mCursor= mDb.rawQuery(dinnersql,null);

        for (int i=0;i<mCursor.getCount();i++){
            mCursor.moveToNext();



            dinnermeal.add(mCursor.getString(1));
            dinnercal.add(mCursor.getString(2));

        }


    }

    public void getMealdata(){
        date= MealLibrary.getDateNew("goe.go.kr", "J100000708", "4", "04","2"); // 날짜
        calarry= MealLibrary.getKcalNew("goe.go.kr", "J100000708", "4", "04", "2"); // 점심 칼로리
        mealarry=  MealLibrary.getMealNew("goe.go.kr", "J100000708", "4", "04", "2");// 점심 식사 목록
        dinnerarry= MealLibrary.getMealNew("goe.go.kr", "J100000708", "4", "04", "3"); // 저녁 식사 목록
        dinnercalarry=MealLibrary.getKcalNew("goe.go.kr", "J100000708", "4", "04", "3"); // 저녁 식사 칼로리





    }

    public void saveinsql(){
                String strsql;

        for (int i=1;i<=5;i++){
            strsql =  "insert into mealist(date,list,kcal,type) values('"+date[i]+"','"+mealarry[i]+"','"+calarry[i]+"','"+"2"+"')";  // type 가 2이면 점심 3이면 석식
            mDb.execSQL(strsql);
        }

        for (int i=1;i<=5;i++){
            strsql =  "insert into mealist(date,list,kcal,type) values('"+date[i]+"','"+dinnerarry[i]+"','"+dinnercalarry[i]+"','"+"3"+"')";  // type 가 2이면 점심 3이면 석식
            mDb.execSQL(strsql);
        }
    }








}
